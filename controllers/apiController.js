const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User_game } = require('../models');

module.exports = {

  login: async (req, res) => {
    try {
      const user = await User_game.findAll({
        where: {
          email: req.body.email,
        },
      });
      const match = await bcrypt.compare(req.body.password, user[0].password);
      if (!match) return res.status(400).json({ msg: 'Wrong Password' });
      const userId = user[0].id;
      const { email } = user[0];
      const accessToken = jwt.sign({ userId, email }, 'rahasia1', {
        expiresIn: '20s',
      });
      const refreshToken = jwt.sign({ userId, email }, 'rahasia2', {
        expiresIn: '1d',
      });
      await User_game.update({ refresh_token: refreshToken }, {
        where: {
          id: userId,
        },
      });
      res.cookie('refreshToken', refreshToken, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000,
      });
      // res.json({ accessToken });
      res.json({
        message: 'Login Success',
        data: user,
        accessToken,
      });
    } catch (error) {
      res.status(404).json({ msg: 'Email tidak ditemukan' });
    }
  },

  register: async (req, res) => {
    const { email, password, username } = req.body;
    if (!email || !password || !username) return res.status(400).json({ message: 'Please fill all the field' });
    const salt = bcrypt.genSaltSync();
    const hash = bcrypt.hashSync(password, salt);
    User_game.create({
      email,
      password: hash,
      username,
      score: 0,
      bio: '',
    }).then((user) => {
      res.json({
        message: 'Register Success',
        data: user,
      });
    }).catch((err) => {
      res.json({
        message: err.message,
      });
    });
  },

  logout: async (req, res) => {
    const { refreshToken } = req.cookies;
    if (!refreshToken) return res.sendStatus(204);
    const user = await User_game.findAll({
      where: {
        refresh_token: refreshToken,
      },
    });
    if (!user[0]) return res.sendStatus(204);
    const userId = user[0].id;
    await User_game.update({ refresh_token: null }, {
      where: {
        id: userId,
      },
    });
    res.clearCookie('refreshToken');
    return res.sendStatus(200);
  },

  getUser: async (req, res) => {
    try {
      const user = await User_game.findByPk(req.params.id);
      if (user) {
        res.json(user);
      } else {
        res.status(404).json({ message: 'User not found!' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: error.message });
    }
  },

  editUser: async (req, res) => {
    try {
      const user = await User_game.findByPk(req.params.id);
      if (user) {
        await user.update({
          username: req.body.username,
          email: req.body.email,
          score: req.body.score,
          url: req.body.url,
          bio: req.body.bio,
        });
        res.json(user);
      } else {
        res.status(404).json({ message: 'User not found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: error.message });
    }
  },

  getAllUser: async (req, res) => {
    try {
      const users = await User_game.findAll();
      res.json(users);
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: error.message });
    }
  },

  rankUser: async (req, res) => {
    try {
      const user = await User_game.findAll({
        order: [
          ['score', 'DESC'],
        ],
        limit: 10,
      });
      if (user) {
        res.json(user);
      } else {
        res.status(404).json({ message: 'User not found!' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: error.message });
    }
  },

  landingPage: async (req, res) => {
    try {
      res.status(200).json({
        message: 'Test API',
      });
    } catch (error) {
      res.status(500).json({ message: 'Internal server error' });
    }
  },
};
