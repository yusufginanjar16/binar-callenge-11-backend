const express = require('express');

const router = express();
const apiController = require('../controllers/apiController');

// Landing page
router.get('/', apiController.landingPage);

// Login Page
router.post('/login', apiController.login);
router.post('/register', apiController.register);
router.delete('/logout', apiController.logout);

// rank user
router.get('/rank', apiController.rankUser);

// router.post('/user/add', apiController.addUser);

router.get('/user/:id', apiController.getUser);
router.post('/user/:id', apiController.editUser);
router.get('/users', apiController.getAllUser);

module.exports = router;
