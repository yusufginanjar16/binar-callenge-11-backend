const request = require('supertest');
const app = require('../app');

describe('Login', () => {
  // test('should return 200 OK', async () => {
  //     const response = await request(app).post('/api/v1/login').send({
  //         email: 'yusuf20@gmail.com',
  //         password: '123456',
  //     });
  //     expect(response.statusCode).toBe(200);
  //     expect(response.body).toHaveProperty('email');
  //     expect(response.body).toHaveProperty('username');
  //     expect(response.body).toHaveProperty('score');
  // });

  test('should return 400 Bad Request', async () => {
    const response = await request(app).post('/api/v1/login').send({
      username: 'test',
      password: 'test',
    });
    expect(response.statusCode).toBe(404);
    expect(response.body).toHaveProperty('msg');
  });
});
