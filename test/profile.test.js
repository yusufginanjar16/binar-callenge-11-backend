const request = require('supertest');
const app = require('../app');

describe('GET /profile', () => {
  test('should return 200 OK', async () => {
    const response = await request(app).get('/api/v1/user/1');
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('username');
    expect(response.body).toHaveProperty('email');
    expect(response.body).toHaveProperty('score');
  });

  test('should return 404 Not Found', async () => {
    const response = await request(app).get('/api/v1/user/100');
    expect(response.statusCode).toBe(404);
    expect(response.body).toHaveProperty('message');
  });
});

describe('POST /profile', () => {
  test('should return 201 Created', async () => {
    const response = await request(app).post('/api/v1/user/1').send({
      username: 'test',
      email: 'test@gmail.com',
      score: 0,
      url: '',
      bio: '',
    });
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('username');
    expect(response.body).toHaveProperty('email');
    expect(response.body).toHaveProperty('score');
  });

  test('should return 400 Bad Request', async () => {
    const response = await request(app).post('/api/v1/user/100').send({
      username: 'test',
      email: 'test@gmail.com',
      score: 0,
      url: '',
      bio: '',
    });
    expect(response.statusCode).toBe(404);
    expect(response.body).toHaveProperty('message');
  });

  test('should return 500 Internal Server Error', async () => {
    const response = await request(app).post('/api/v1/user/1').send({
      score: 'string',
    });
    expect(response.statusCode).toBe(500);
    expect(response.body).toHaveProperty('message');
  });
});
