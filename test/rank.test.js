const request = require('supertest');
const app = require('../app');

describe('GET /rank', () => {
  test('should return 200 OK', async () => {
    const response = await request(app).get('/api/v1/rank');
    expect(response.statusCode).toBe(200);
  });

  test('should return 404 Not Found', async () => {
    const response = await request(app).get('/api/v1/rank/1');
    expect(response.statusCode).toBe(404);
  });
});
