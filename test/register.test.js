const request = require('supertest');
const app = require('../app');

describe('Register', () => {
  test('should return 200 Created', async () => {
    const response = await request(app).post('/api/v1/register').send({
      username: 'test',
      email: 'test@gmail.com',
      password: '123456',
    });
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('data');
    expect(response.body).toHaveProperty('message');
  });

  test('should return 400 Bad Request', async () => {
    const response = await request(app).post('/api/v1/register').send({
      username: 'test',
      password: 'test',
    });
    expect(response.statusCode).toBe(400);
    expect(response.body).toHaveProperty('message');
  });
});
