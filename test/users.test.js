const request = require('supertest');
const app = require('../app');

describe('GET /users', () => {
  test('should return 200 OK', async () => {
    const response = await request(app).get('/api/v1/users');
    expect(response.statusCode).toBe(200);
  });

  test('should return 404 Not Found', async () => {
    const response = await request(app).get('/api/v1/users/1');
    expect(response.statusCode).toBe(404);
  });
});
